package by.itstep.linguaapp;

import by.itstep.linguaapp.utils.DBCleaner;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@AutoConfigureMockMvc
public class LinguaapplicationTest {

    public static final Faker FAKER = new Faker();

    @Autowired
    private DBCleaner dbCleaner;

    @BeforeEach
    public void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    public void shutUp() {
        dbCleaner.clean();
    }


}
