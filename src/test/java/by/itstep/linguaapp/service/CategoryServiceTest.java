package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CategoryServiceTest {

    @Autowired
    private CategoryService categoryService;


    @Test
    public void create_HappyPath(){
        //given
        boolean newDTO = false;
        CategoryCreateDto existDto = new CategoryCreateDto();
        existDto.setName("bob");

        CategoryCreateDto dto = new CategoryCreateDto();
        dto.setName("bob");
        //when

        try {
            categoryService.create(dto);
        } catch (RuntimeException ex) {
            newDTO = true;
        }
        //then
        Assertions.assertTrue(newDTO);

    }
}
