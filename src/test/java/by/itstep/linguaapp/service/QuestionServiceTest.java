package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.*;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import com.github.javafaker.Bool;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@SpringBootTest

public class QuestionServiceTest {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private QuestionMapper questionMapper;

    @MockBean
    private AuthenticationService authenticationService;
/*
* INSERT INTO users (email, name, phone, role, password, country, blocked, deleted_at)
VALUES ('admin@gmail.com', 'admin', '1231231', 'ADMIN', '$2a$10$UENRrmiqB1/BD0LTlzoIAuffIWU3aKPYzNNQl83iufrFQ7jQmVEs.', 'BY', false, null);
*/


    @BeforeEach
    public void SetUp(){
        answerRepository.deleteAllInBatch();
        questionRepository.deleteAllInBatch();
        categoryRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
    }

    @Test
    public void findAll_happyPathWithSoftDelete() {
        // given
        CategoryEntity category = addCategoryToDb();
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());

        QuestionFullDto question = addQuestionToDb(category.getId());
        questionService.delete(question.getId()); // Удаляем 1 из 5 вопросов

        // when
        List<QuestionShortDto> foundQuestions = questionService.findAll();

        // then
        Assertions.assertEquals(4, foundQuestions.size());
    }


    @Test
    @Transactional
    public void create_happyPath() {
        // given
        CategoryEntity existingCategory = addCategoryToDb();
        List<Integer> categoryIds = Arrays.asList(existingCategory.getId());

        QuestionCreateDto createDto = generateQuestionCreateDto(categoryIds);

        // when
        QuestionFullDto created = questionService.create(createDto);
        // then
        Assertions.assertNotNull(created);
        Assertions.assertNotNull(created.getId());

        QuestionEntity createdEntity = questionRepository.getById(created.getId());
        Assertions.assertEquals(createdEntity.getAnswers().size(),
        createDto.getAnswers().size());

        for(AnswerEntity savedAnswer : createdEntity.getAnswers()){
            Assertions.assertNotNull(savedAnswer.getId());
        }
        Assertions.assertEquals(createdEntity.getCategories().size(),
                createDto.getCategoryIds().size());
    }


    @Test
    @Transactional
    public void getById_happyPath (){
        //given
        CategoryEntity existingCategory = addCategoryToDb();
        List<Integer> categoryIds = Arrays.asList(existingCategory.getId());

        QuestionFullDto questionDto = addQuestionToDb(categoryIds);
        //when
        QuestionFullDto findQuestion = questionService.getById(questionDto.getId());
        //than
        Assertions.assertEquals(findQuestion.getId(), questionDto.getId());

    }

    @Test
    @Transactional
    public void findAll_happyPath(){
        //given
        CategoryEntity existingCategory = addCategoryToDb();
        List<Integer> categoryIds = Arrays.asList(existingCategory.getId());

        addQuestionToDb(categoryIds);
        //when
        List<QuestionShortDto> allQuestion = questionService.findAll();
        //than
        Assertions.assertEquals(1, allQuestion.size());
    }

    @Test
    @Transactional
    public void update_happyPath(){
        //given
        CategoryEntity existingCategory = addCategoryToDb();
        List<Integer> categoryIds = Arrays.asList(existingCategory.getId());
        QuestionFullDto existingQuestion = addQuestionToDb(categoryIds);

        QuestionUpdateDto questionUpdate = new QuestionUpdateDto();
        questionUpdate.setId(existingQuestion.getId());
        questionUpdate.setDescription("test-test");
        questionUpdate.setLevel(existingQuestion.getLevel());


        //when
        QuestionFullDto updatedQuestion = questionService.update(questionUpdate);
        //than
        Assertions.assertEquals(existingQuestion.getId(), updatedQuestion.getId());

        QuestionEntity questionAfterUpdate = questionRepository.getById(existingQuestion.getId());
        Assertions.assertEquals(questionAfterUpdate.getDescription(), updatedQuestion.getDescription());
        Assertions.assertEquals(questionAfterUpdate.getLevel(), updatedQuestion.getLevel());
    }

    @Test
    @Transactional
    public void getRandomQuestion_happyPath(){
        //given
        CategoryEntity firstCategory = addCategoryToDb();
        CategoryEntity secondCategory = addCategoryToDb();

        QuestionFullDto firstQuestion = addQuestionToDb(firstCategory.getId());

        QuestionFullDto secondQuestion = addQuestionToDb(secondCategory.getId());
        QuestionFullDto thirdQuestion = addQuestionToDb(secondCategory.getId());
        UserEntity user = addUserToDb();

        Mockito.when(authenticationService.getAuthenticationUser()).thenReturn(user);
        //when
        QuestionFullDto foundQuestion = questionService.getRandomQuestion(firstCategory.getId());

        //then3
        Assertions.assertEquals(firstQuestion.getId(), foundQuestion.getId());
    }

    @Test
    @Transactional
    public void getRandomQuestion_whenOneQuestionCompleted(){
        //given
        CategoryEntity firstCategory = addCategoryToDb();
        CategoryEntity secondCategory = addCategoryToDb();

        QuestionFullDto firstQuestion = addQuestionToDb(firstCategory.getId());
        QuestionFullDto secondQuestion = addQuestionToDb(firstCategory.getId());

        QuestionFullDto thirdQuestion = addQuestionToDb(secondCategory.getId());
        QuestionFullDto fourthQuestion = addQuestionToDb(secondCategory.getId());

        UserEntity user = addUserToDb();
        Integer correctAnswerId = null;
        for(AnswerFullDto answer : secondQuestion.getAnswers()) {
            if(answer.getCorrect()){
                correctAnswerId = answer.getId();
            }
        }

        Mockito.when(authenticationService.getAuthenticationUser()).thenReturn(user);

        questionService.checkAnswer(secondQuestion.getId(),correctAnswerId);
        //when
        QuestionFullDto foundQuestion = questionService.getRandomQuestion(firstCategory.getId());

        //then
        Assertions.assertEquals(firstQuestion.getId(), foundQuestion.getId());
    }

    private UserEntity addUserToDb() {
        UserEntity user = new UserEntity();
        user.setBlocked(false);
        user.setEmail("test-email");
        user.setPhone("test-phone");
        user.setPassword("qwerty");
        user.setCountry("BY");
        user.setName("Bob");
        user.setRole(UserRole.USER);

        return userRepository.save(user);
    }

    private QuestionFullDto addQuestionToDb(Integer categoryId) {
        QuestionCreateDto createDto = generateQuestionCreateDto(Arrays.asList(categoryId));
    return questionService.create(createDto);
    }

    private QuestionEntity generateQuestion(List<CategoryEntity> categoryEntities){
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setDescription("test_test");
        questionEntity.setLevel(QuestionLevel.A2);
        questionEntity.setCategories(categoryEntities);
        List<AnswerEntity> answers = Arrays.asList(
                generateAnswer(false),
                generateAnswer(true),
                generateAnswer(false),
                generateAnswer(false)
        );
        questionEntity.setAnswers(answers);
        return questionEntity;
    }

    private QuestionCreateDto generateQuestionCreateDto(List<Integer> categoryIds) {
        QuestionCreateDto questionCreateDto = new QuestionCreateDto();
        questionCreateDto.setDescription("test-description");
        questionCreateDto.setLevel(QuestionLevel.B2);
        questionCreateDto.setCategoryIds(categoryIds);

        List<AnswerCreateDto> answers = Arrays.asList(
                generateAnswerCreateDto(false),
                generateAnswerCreateDto(true),
                generateAnswerCreateDto(false),
                generateAnswerCreateDto(false)
        );
        questionCreateDto.setAnswers(answers);

        return questionCreateDto;
    }

    private QuestionFullDto addQuestionToDb(List<Integer> categoryIds) {
        QuestionCreateDto createDto = generateQuestionCreateDto(categoryIds);
        return questionService.create(createDto);
    }

    private AnswerEntity generateAnswer(Boolean correct) {
        AnswerEntity answerEntity = new AnswerEntity();
        answerEntity.setBody("test_body");
        answerEntity.setCorrect(correct);

        return answerEntity;
    }

    private AnswerCreateDto generateAnswerCreateDto(Boolean correct) {
        AnswerCreateDto answerCreateDto = new AnswerCreateDto();
        answerCreateDto.setBody("test-body");
        answerCreateDto.setCorrect(correct);

        return answerCreateDto;
    }

    private CategoryEntity addCategoryToDb() {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName("test-category");
        return categoryRepository.save(categoryEntity);
    }
}