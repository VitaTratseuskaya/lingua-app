package by.itstep.linguaapp.utils;

import by.itstep.linguaapp.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtHelper {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public String createToken(String email){
        return "Bearer " + jwtTokenProvider.createToken(email);
    }
}
