package by.itstep.linguaapp.utils;

import by.itstep.linguaapp.dto.user.UserCreateDTO;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import static by.itstep.linguaapp.LinguaapplicationTest.FAKER;

public class DtoUserHelper {

    public static UserCreateDTO generateUserCreateDto() {
        UserCreateDTO createDto = new UserCreateDTO();
        createDto.setEmail(FAKER.internet().emailAddress());
        createDto.setName(FAKER.name().firstName());
        createDto.setPassword(FAKER.phoneNumber().cellPhone());
        createDto.setCountry(FAKER.address().country());
        createDto.setPassword(FAKER.internet().password());
        return createDto;
    }

}
