package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.user.UserFullDTO;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void mapToDto_happyPath(){
        //given
        UserEntity user = new UserEntity();
        user.setId(15);
        user.setCountry("BY");
        user.setRole(UserRole.USER);
        user.setEmail("bob@gmail.com");
        user.setName("bob");
        user.setPhone("123 456 789");
        user.setPassword("6666666");
        //when
        UserFullDTO dto = userMapper.mapFullDto(user);
        //then
        Assertions.assertEquals(user.getId(), dto.getId());
        Assertions.assertEquals(user.getEmail(), dto.getEmail());
        System.out.println("---> " + dto);
    }
}
