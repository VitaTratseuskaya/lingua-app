package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.LinguaappApplication;
import by.itstep.linguaapp.LinguaapplicationTest;
import by.itstep.linguaapp.dto.user.UserCreateDTO;
import by.itstep.linguaapp.dto.user.UserFullDTO;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.utils.DBUserHelper;
import by.itstep.linguaapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static by.itstep.linguaapp.utils.DtoUserHelper.generateUserCreateDto;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UserControllerTest extends LinguaapplicationTest {

    @Autowired
    private DBUserHelper dbUserHelper;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findById_happyPath() throws Exception{
        //given
        UserEntity existingUser = dbUserHelper.addUserToDb(UserRole.USER);

        UserEntity currentUser = dbUserHelper.addUserToDb(UserRole.ADMIN);

        String token = jwtHelper.createToken(currentUser.getEmail());


        //when
        MvcResult mvcResult = mockMvc.perform(request
                (GET,"/users/" + existingUser.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDTO foundUser = objectMapper.readValue(bytes, UserFullDTO.class);

        //then
        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
        System.out.println("->>> " + foundUser);
    }

    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        // given
        UserEntity existingUser = dbUserHelper.addUserToDb(UserRole.USER);

        // when
        mockMvc.perform(request(GET, "/users/" + existingUser.getId()))
                .andExpect(status().isForbidden());
    }


    @Test
    public void create_happyPath() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.addUserToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserCreateDTO createDto = generateUserCreateDto();

        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDTO foundUser = objectMapper.readValue(bytes, UserFullDTO.class);


        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(UserRole.USER, foundUser.getRole());

    }

    @Test
    public void create_whenNotAuthenticated() throws Exception {
        //given
        UserCreateDTO createDto = generateUserCreateDto();

        //when
        mockMvc.perform(request(POST, "/users")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden()); // when not 401
    }


    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.addUserToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserCreateDTO createDto = generateUserCreateDto();

        //when
        mockMvc.perform(request(POST, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isForbidden());
    }
}
