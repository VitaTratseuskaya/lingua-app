package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.jvm.hotspot.debugger.Page;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {


    @Autowired
    private UserService userService;

    @PostMapping("/users")
    public UserFullDTO create(@Valid @RequestBody UserCreateDTO dto) {
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDTO update(@Valid @RequestBody UserUpdateDTO dto) {
        return userService.update(dto);
    }

    @GetMapping("/users/{id}")
    public UserFullDTO findById( @PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public Page<UserFullDTO> findAll(@RequestParam int page, @RequestParam int size) {
        return userService.findAll(page, size);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id) {
        userService.delete(id);
    }

    @PutMapping("/users/password")
    public void changePassword(@Valid @RequestBody ChangeUserPasswordDto dto) {
        userService.changePassword(dto);
    }

    @PutMapping("/users/role")
    public UserFullDTO changeRole(@Valid @RequestBody ChangeUserRole dto) {
        return userService.changeRole(dto);
    }

    @PutMapping(path = "/users/{id}/block")
    public void blocked(
            @PathVariable Integer id,
            @RequestHeader("Authorization") String authorization
    ) throws Exception {
        userService.block(id);
    }



}
