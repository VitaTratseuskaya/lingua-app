package by.itstep.linguaapp.exeption;

public class BusinessLogicException extends RuntimeException {

    public BusinessLogicException(String message){
        super(message);
    }
}
