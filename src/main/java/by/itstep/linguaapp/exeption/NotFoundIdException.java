package by.itstep.linguaapp.exeption;

public class NotFoundIdException extends RuntimeException{

    public NotFoundIdException(String message) {
        super(message);
    }

}
