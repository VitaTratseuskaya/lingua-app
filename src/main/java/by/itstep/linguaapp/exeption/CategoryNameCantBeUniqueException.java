package by.itstep.linguaapp.exeption;

public class CategoryNameCantBeUniqueException extends RuntimeException {

    public CategoryNameCantBeUniqueException(String message) {
        super(message);
    }
}
