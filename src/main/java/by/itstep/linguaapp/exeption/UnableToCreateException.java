package by.itstep.linguaapp.exeption;

public class UnableToCreateException extends RuntimeException{

    public UnableToCreateException(String message) {
        super(message);
    }
}
