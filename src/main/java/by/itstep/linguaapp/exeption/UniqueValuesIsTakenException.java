package by.itstep.linguaapp.exeption;

public class UniqueValuesIsTakenException extends RuntimeException {

    public UniqueValuesIsTakenException(String message) {
        super(message);
    }
}