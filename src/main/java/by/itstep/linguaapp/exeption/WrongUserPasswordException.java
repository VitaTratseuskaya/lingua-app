package by.itstep.linguaapp.exeption;

public class WrongUserPasswordException extends RuntimeException {

    public WrongUserPasswordException(String message) {
        super(message);
    }
}
