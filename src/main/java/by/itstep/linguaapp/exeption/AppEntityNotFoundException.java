package by.itstep.linguaapp.exeption;

public class AppEntityNotFoundException extends RuntimeException {

    public AppEntityNotFoundException(String message) {
        super(message);
    }
}

