package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends JpaRepository <UserEntity, Integer> {

    @Query(value = "SELECT * FROM users WHERE deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findAll();

    Optional<UserEntity> findByEmail(String email);

    @Query(value = "SELECT * FROM users WHERE role = 'ADMIN' AND deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findAllAdmins();

    @Query(value = "SELECT * FROM users WHERE email LIKE %:d AND deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findAllByEmailDomain(@Param("d") String domain);

    @Modifying
    @Query(value = "UPDATE users SET deleted_at =NOW() WHERE role = 'ADMIN'", nativeQuery = true)
    void deleteAllAdmins();
}
