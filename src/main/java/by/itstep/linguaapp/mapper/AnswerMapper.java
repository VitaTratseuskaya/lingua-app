package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AnswerMapper {

    AnswerEntity map(AnswerCreateDto dto);

    List<AnswerFullDto> map(List<AnswerEntity> entity);

    AnswerFullDto map(AnswerEntity entity);
}
