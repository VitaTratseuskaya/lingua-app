package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.user.UserCreateDTO;
import by.itstep.linguaapp.dto.user.UserFullDTO;
import by.itstep.linguaapp.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(componentModel = "spring")
public interface UserMapper {
//@Mapping(target = "phoneNumber", source = "phone") - если поля отличаются в названии



    UserEntity mapEntity(UserCreateDTO user);

  //  @Mapping(target = "phone", expression = "java(entity.getPhone().split(\" \"))")
    UserFullDTO mapFullDto(UserEntity entity);

    List<UserFullDTO> map(List<UserEntity> entities);


}
