package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.user.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    UserFullDTO create(UserCreateDTO dto);

    UserFullDTO update(UserUpdateDTO dto);

    UserFullDTO findById (int id);

    Page<UserFullDTO> findAll(int page, int size);

    void delete(Integer id);

    void changePassword(ChangeUserPasswordDto dto);

    UserFullDTO changeRole(ChangeUserRole dto);

    void block(Integer userId);
}
