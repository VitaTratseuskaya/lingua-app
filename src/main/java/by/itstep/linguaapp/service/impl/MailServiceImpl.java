package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;
    @Override
    public void sendEmail(String email, String message) {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setSubject("Email from LinguaApp Team");
        mail.setTo(email);
        mail.setText("Text from Tratseuskaya Vita: " + message);

        mailSender.send(mail);

    }
}
