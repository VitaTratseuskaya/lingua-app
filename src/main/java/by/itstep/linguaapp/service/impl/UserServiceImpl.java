package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.exeption.AppEntityNotFoundException;
import by.itstep.linguaapp.exeption.UniqueValuesIsTakenException;
import by.itstep.linguaapp.exeption.WrongUserPasswordException;
import by.itstep.linguaapp.mapper.UserMapper;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDTO create(UserCreateDTO dto) {
        UserEntity entityToSave = userMapper.mapEntity(dto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(UserRole.USER);
        entityToSave.setPassword(passwordEncoder.encode(dto.getPassword() + dto.getEmail()));

        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(entityToSave.getEmail());
        if(entityWithSameEmail.isPresent()) {
            throw  new UniqueValuesIsTakenException("Email is taken!");
        }

        UserEntity savedEntity = userRepository.save(entityToSave);
        log.info("User was successfully created");
        return userMapper.mapFullDto(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDTO update(UserUpdateDTO dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setCountry(dto.getCountry());
        userToUpdate.setName(dto.getName());
        userToUpdate.setPhone(dto.getPhone());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        log.info("User was successfully updated");
        return userMapper.mapFullDto(updatedUser);
    }

    @Override
    @Transactional
    public UserFullDTO findById(int id) {
        UserEntity foundUser = userRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        log.info("User was successfully found");
        return userMapper.mapFullDto(foundUser);
    }

    @Override
    @Transactional
    public Page<UserFullDTO> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Page<UserEntity> foundEntities = userRepository.findAll(pageable);

        Page<UserFullDTO> dtos = foundEntities.map(entity -> userMapper.map(entity));

        log.info(dtos.getNumberOfElements() + " users were found");
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        UserEntity foundUser = userRepository.getById(id);
        if (foundUser == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + id);
        }

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);

        log.info("User was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(ChangeUserPasswordDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getUserId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId());
        }

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!userToUpdate.getPassword().equals(passwordEncoder.encode(dto.getOldPassword() + userToUpdate.getEmail()))) {
            throw new WrongUserPasswordException("Wrong password");
        }

        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getEmail()));
        userRepository.save(userToUpdate);
        log.info("User was successfully updated");

    }

    @Override
    @Transactional
    public UserFullDTO changeRole(ChangeUserRole dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getUserId());

        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId());
        }

        userToUpdate.setRole(dto.getNewRole());
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDTO userDto = userMapper.mapFullDto(updatedUser);
        log.info("User was successfully updated");

        return userDto;
    }

    @Override
    @Transactional
    public void block(Integer userId) {
        UserEntity userToUpdate = userRepository.getById(userId);
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + userId);
        }
        userToUpdate.setBlocked(true);
        userRepository.save(userToUpdate);

        log.info("User was successfully updated");
    }

    private boolean currentUserIsAdmin() {
            return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .filter(ga -> ga.getAuthority().equals("ROLE_" + UserRole.ADMIN.name()))
                .findAny()
                .isPresent();
    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }


}

