package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.*;
import by.itstep.linguaapp.exeption.AppEntityNotFoundException;
import by.itstep.linguaapp.exeption.NotFoundIdException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private QuestionMapper questionMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private MailServiceImpl mailService;

    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto dto) {
        throwIfInvalidNumberOfCorrectAnswers(dto);

        QuestionEntity questionToSave = questionMapper.map(dto);
        for (AnswerEntity answer : questionToSave.getAnswers()) {
            answer.setQuestion(questionToSave);
        }

        List<CategoryEntity> categoriesToAdd = categoryRepository.findAllById(dto.getCategoryIds());
        questionToSave.setCategories(categoriesToAdd);

        QuestionEntity savedQuestion = questionRepository.save(questionToSave);

        QuestionFullDto questionDto = questionMapper.map(savedQuestion);
        System.out.println("QuestionService -> Question was successfully created");

        return questionDto;
    }

    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto dto) {
        QuestionEntity questionForUpdate = questionRepository.getById(dto.getId());
        if (dto == null) {
            throw new NotFoundIdException("Dot not found by id " + questionForUpdate.getId());
        }

        questionForUpdate.setDescription(dto.getDescription());
        questionForUpdate.setLevel(dto.getLevel());

        QuestionEntity updatedQuestion = questionRepository.save(questionForUpdate);
        System.out.println("QuestionService -> Found for update question -> " + updatedQuestion);

        return questionMapper.map(updatedQuestion);
    }

    @Override
    @Transactional
    public QuestionFullDto getById(Integer id) {
        QuestionEntity question = questionRepository.getById(id);

        if (question == null) {
            throw new NotFoundIdException("QuestionService -> Question not found by ID.");
        }
        System.out.println("QuestionService -> Found question: " + question);

        return questionMapper.map(question);
    }

    @Override
    @Transactional
    public List<QuestionShortDto> findAll() {
        List<QuestionEntity> allQuestion = questionRepository.findAll();
        System.out.println("QuestionService -> Find all question" + allQuestion);
        return questionMapper.map(allQuestion);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        QuestionEntity questionForDelete = questionRepository.getById(id);
        if(questionForDelete == null) {
            throw  new NotFoundIdException("Question was not found by id");
        }
        questionForDelete.setDeletedAt(Instant.now());
        for(AnswerEntity answer : questionForDelete.getAnswers()) {
            answer.setDeletedAt((Instant.now()));
        }
        questionRepository.save(questionForDelete);

        System.out.println("QuestionServiceImpl ->  Question was successfully deleted");
    }

    @Override
    @Transactional
    public boolean checkAnswer(Integer questionId, Integer answerId) {
        AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId);
        if (answer == null) {
            throw new AppEntityNotFoundException("Answer was not found by id: "
                    + answerId + " in the question: " + questionId);
        }

        if(answer.getCorrect()){
            QuestionEntity question = answer.getQuestion();
            UserEntity user = authenticationService.getAuthenticationUser();

            question.getUserWhoCompleted().add(user); //добавили юзера в спиок прошедших этот вопрос
            questionRepository.save(question);
            mailService.sendEmail(user.getEmail(), "Good job! Right answer!");
        }
        return answer.getCorrect();
    }
    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(Integer categoryId) {
        UserEntity user = authenticationService.getAuthenticationUser();

        List<QuestionEntity> foundQuestions =
                questionRepository.findNotCompleted(categoryId, user.getId());

        if(foundQuestions.isEmpty()) {
            throw new AppEntityNotFoundException("Can't find available question by category id: " + categoryId);
        }

        int randomIndex = (int)(foundQuestions.size() * Math.random());
        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        System.out.println("QuestionServiceImpl -> ");

        return questionMapper.map(randomQuestion);
    }

    private void throwIfInvalidNumberOfCorrectAnswers(QuestionCreateDto dto) {
        int counter = 0;
        for (AnswerCreateDto answer : dto.getAnswers()) {
            if (answer.getCorrect()) {
                counter++;
            }
        }
        if (counter != 1) {
            throw new ValidationException("Question must contains the only one correct answer");
        }
    }
}
