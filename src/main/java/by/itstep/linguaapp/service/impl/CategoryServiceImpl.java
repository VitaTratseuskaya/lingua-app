package by.itstep.linguaapp.service.impl;


import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.exeption.AppEntityNotFoundException;
import by.itstep.linguaapp.exeption.UnableToCreateException;
import by.itstep.linguaapp.exeption.UniqueValuesIsTakenException;
import by.itstep.linguaapp.mapper.CategoryMapper;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    @Transactional
    public CategoryFullDto create(CategoryCreateDto dto) {
        CategoryEntity entityToSave = categoryMapper.map(dto);

        if (entityToSave.getId() != null) {
            throw new UniqueValuesIsTakenException("Can't create category with id");
        }

        CategoryEntity savedEntity = categoryRepository.save(entityToSave);
        System.out.println("CategoryService -> Category was successfully created");
        return categoryMapper.map(savedEntity);
    }

    @Override
    @Transactional
    public CategoryFullDto update(CategoryUpdateDto dto) {
        CategoryEntity categoryToUpdate = categoryRepository.getById(dto.getId());
        if (categoryToUpdate == null) {
            throw new AppEntityNotFoundException("CategoryEntity was not found by id: " + dto.getId());
        }
        categoryToUpdate.setName(dto.getName());

        CategoryEntity updatedCategory = categoryRepository.save(categoryToUpdate);
        System.out.println("CategoryServiceImpl -> Category was successfully updated");
        return categoryMapper.map(updatedCategory);
    }

    @Override
    @Transactional
    public CategoryFullDto findById(int id) {
        CategoryEntity foundCategory = categoryRepository.getById(id);
        if (foundCategory == null) {
            throw new AppEntityNotFoundException("CategoryEntity was not found by id: " + id);
        }
        System.out.println("CategoryServiceImpl -> Category was successfully found");
        return categoryMapper.map(foundCategory);
    }

    @Override
    @Transactional
    public List<CategoryFullDto> findAll() {
        List<CategoryEntity> foundCategory = categoryRepository.findAll();
        List<CategoryFullDto> dtos = categoryMapper.map(foundCategory);
        System.out.println("CategoryServiceImpl -> " + dtos.size() + " category were found");

        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        CategoryEntity foundCategory = categoryRepository.getById(id);
        if (foundCategory == null) {
            throw new AppEntityNotFoundException("CategoryEntity was not found by id: " + id);
        }
        foundCategory.setDeletedAt(Instant.now());
        categoryRepository.save(foundCategory);

        System.out.println("CategoryServiceImpl ->  Category was successfully deleted");
    }
}
