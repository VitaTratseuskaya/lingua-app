package by.itstep.linguaapp.dto.category;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CategoryUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;
}
