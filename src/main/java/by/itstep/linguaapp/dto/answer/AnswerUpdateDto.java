package by.itstep.linguaapp.dto.answer;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AnswerUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String body;

    @NotNull
    private Boolean correct;
}
