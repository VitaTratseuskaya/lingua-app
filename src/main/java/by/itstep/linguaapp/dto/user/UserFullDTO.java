package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDTO {
    //лучше создавать enum's и в пакете дто (копирнуть)

    private Integer id;
    private String name;
    private String email;
    private UserRole role;
    private String country;
    private String phone;
}
