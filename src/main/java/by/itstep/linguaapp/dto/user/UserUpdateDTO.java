package by.itstep.linguaapp.dto.user;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDTO {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    @Size(min = 2, max = 2)
    private String country;

    @Size(min = 7, max = 20)
    private String phone;
}
