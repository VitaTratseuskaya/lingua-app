package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserCreateDTO {

    @NotBlank
    private String name;

    @Email(message = "Email can not be null")
    private String email;

    @Size(min = 8)
    @NotBlank
    private String password;

    @NotBlank
    @Size(min = 2, max = 2)
    private String country;

    @Size(min = 7, max = 20)
    private String phone;
}
