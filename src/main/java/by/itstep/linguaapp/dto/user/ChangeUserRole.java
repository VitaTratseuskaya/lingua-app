package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class ChangeUserRole {

    @NotNull
    private Integer userId;

    @NotNull
    private UserRole newRole;
}
