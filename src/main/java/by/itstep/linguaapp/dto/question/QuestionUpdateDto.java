package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.entity.QuestionLevel;
import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class QuestionUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String description;

    @NotNull
    private QuestionLevel level;
}
